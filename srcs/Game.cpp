#include "Game.h"
#include "Block.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>

Game::Game(int width, int height, int bombCount)
{
	this->width = width;
	this->height = height;
	this->bombCount = bombCount;

	scale = 2;
	bPendingKill = false;
	Init();
}

Game::~Game()
{
	FreeBlocks();
	delwin(win);
}

void Game::InitWin()
{
	initscr();
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_BLUE, COLOR_BLACK);
	init_pair(3, COLOR_CYAN, COLOR_BLACK);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);
	noecho();
	cbreak();
	curs_set(0);
	width += 1;
	width *= scale;
	height += 2;
	win = newwin(height, width + 1, LINES / 2 - height / 2, COLS / 2 - (width + 1) / 2);
	width /= scale;
	width -= 1;
	height -= 2;
	box(win, 0, 0);
	keypad(win, TRUE);
	wrefresh(win);
}

void Game::Update()
{
	int ch = wgetch(win);

	if (ch == 27)
	{
		Close();
		return;
	}

	if (!bEnd)
	{
		if (ch == KEY_RIGHT)
			xSelect++;
		if (ch == KEY_LEFT)
			xSelect--;
		if (ch == KEY_UP)
			ySelect--;
		if (ch == KEY_DOWN)
			ySelect++;

		if (xSelect < 0)
			xSelect = width - 1;
		if (xSelect > width - 1)
			xSelect = 0;
		if (ySelect < 0)
			ySelect = height - 1;
		if (ySelect > height - 1)
			ySelect = 0;

		Block* block = GetSelectBlock();

		if (ch == 10 && !block->IsAFlag())
		{
			if (!bGenerate)
			{
				Generate();
				bGenerate = true;
			}
			if (block->IsABomb())
			{
				attron(COLOR_PAIR(1));
				wattron(win, COLOR_PAIR(1));
				End("You loose !");
				attroff(COLOR_PAIR(1));
				wattroff(win, COLOR_PAIR(1));
				bLost = true;
			}
			else
			{
				block->Discover();
				CheckWin();
			}
		}
		else if (ch == ' ' && block->GetBlockType() == EHidden)
			block->Flag();
	}
	else if (ch == 10)
		Restart();
}

void Game::Draw()
{
	int xx;
	int yy = 1;

	for (int y = 0; y < height; y++)
	{
		xx = 2;
		for (int x = 0; x < width; x++)
		{
			if (x == xSelect && y == ySelect)
				wattron(win, A_REVERSE);

			Block* block = GetBlock(x, y);
			if (block->GetBlockType() == EHidden)
			{
				if (block->IsABomb() && bLost)
				{
					if (block->IsAFlag())
						wattron(win, COLOR_PAIR(2));
					else
						wattron(win, COLOR_PAIR(1));
					mvwprintw(win, yy, xx, "x");
					wattroff(win, COLOR_PAIR(1));
					wattroff(win, COLOR_PAIR(2));
				}
				else if (block->IsAFlag())
				{
					wattron(win, COLOR_PAIR(2));
					mvwprintw(win, yy, xx, "!");
					wattroff(win, COLOR_PAIR(2));
				}
				else
					mvwprintw(win, yy, xx, "#");
			}
			else if (block->GetBlockType() == EDiscover)
			{
				if (block->GetBombCount() > 0)
				{
					wattron(win, COLOR_PAIR(3));
					mvwprintw(win, yy, xx, "%d", block->GetBombCount());
					wattroff(win, COLOR_PAIR(3));
				}
				else
					mvwprintw(win, yy, xx, ".");
			}
			wattroff(win, A_REVERSE);
			xx += 2;
		}
		yy++;
	}
	wrefresh(win);
}

Block* Game::GetBlock(int x, int y)
{
	if (x < 0 || x > width - 1 || y < 0 || y > height - 1)
		return NULL;
	return &blocks[x][y];
}

Block* Game::GetSelectBlock()
{
	return GetBlock(xSelect, ySelect);
}

void Game::Generate()
{
	if (bombCount >= width * height)
		bombCount = width * height - 1;
	if (bombCount <= 0)
		bombCount = 1;
	int count = bombCount;
	do
	{
		int x = RandomRange(0, width);
		int y = RandomRange(0, height);
		Block* block = GetBlock(x, y);
		if (block && block != GetSelectBlock() && !block->IsABomb())
		{
			block->SetAsBomb();
			count--;
		}
	}while(count > 0);

	for (int y = 0; y < height; y++)
	{

		for (int x = 0; x < width; x++)
		{
			Block* block = GetBlock(x, y);
			if (!block->IsABomb())
				block->UpdateBombCount();
		}
	}
}

int Game::RandomRange(int min, int max)
{
	int num = max - min;
	return rand() % num + min; 
}

void Game::End(const char* message)
{
	bEnd = true;
	int len = strlen(message);
	mvprintw(LINES / 2 - height / 2 - 5, COLS / 2 - len / 2, message);
	const char* keyMessage = "Press 'enter' to restart.";
	len = strlen(keyMessage);
	mvprintw(LINES / 2 - height / 2 - 3, COLS / 2 - len / 2, keyMessage);
	refresh();
	box(win, 0, 0);
}

void Game::Restart()
{
	clear();
	refresh();
	box(win, 0, 0);
	FreeBlocks();
	Init();
}

void Game::Init()
{
	srand(clock() * clock());
	bLost = false;
	bEnd = false;
	bGenerate = false;
	xSelect = width / 2;
	ySelect = height / 2;
	blocks = new Block*[width];
	for (int x = 0; x < width; x++)
	{
		blocks[x] = new Block[height];
		for (int y = 0; y < height; y++)
			blocks[x][y].SetGameAndIndex(this, x, y);
	}
}

void Game::CheckWin()
{
	int count = 0;
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			Block* block = GetBlock(x, y);
			if (block->GetBlockType() == EHidden)
				count++;
		}
	}
	if (count == bombCount)
	{
		attron(COLOR_PAIR(4));
		wattron(win, COLOR_PAIR(4));
		End("You win !");
		attroff(COLOR_PAIR(4));
		wattroff(win, COLOR_PAIR(4));
	}
}

void Game::Close()
{
	bPendingKill = true;
}

bool Game::IsPendingKill()
{
	return bPendingKill;
}

void Game::FreeBlocks()
{
	for (int i = 0; i < width; i++)
		delete [] blocks[i];
	delete [] blocks;
}
