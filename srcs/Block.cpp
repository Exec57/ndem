#include "Block.h"
#include "Game.h"

Block::Block()
{
	blockType = EHidden;
	bombCount = 0;
	bIsABomb = false;
	bIsAFlag = false;
}

void Block::SetGameAndIndex(Game* game, int x, int y)
{
	this->game = game;
	this->x = x;
	this->y = y;
}

void Block::SetBlockType(EBlockType blockType)
{
	this->blockType = blockType;
}

EBlockType Block::GetBlockType()
{
	return blockType;
}

void Block::SetAsBomb()
{
	bIsABomb = true;
}

bool Block::IsABomb()
{
	return bIsABomb;
}

void Block::UpdateBombCount()
{
	for (int y = -1 ; y <= 1; y++)
	{
		for (int x = -1; x <= 1; x++)
		{
			if (x == 0 && y == 0)
				continue;

			int xx = this->x + x;
			int yy = this->y + y;

			Block *block = game->GetBlock(xx, yy);
			if (block && block->IsABomb())
				bombCount++;
		}
	}
}

int	Block::GetBombCount()
{
	return bombCount;
}

void Block::Discover()
{
	blockType = EDiscover;
	if (bombCount == 0)
	{
		for (int y = -1 ; y <= 1; y++)
		{
			for (int x = -1; x <= 1; x++)
			{
				if (x == 0 && y == 0)
					continue;

				int xx = this->x + x;
				int yy = this->y + y;

				Block* block = game->GetBlock(xx, yy);
				if (block && block->GetBlockType() == EHidden && !block->IsABomb() &&
						!block->IsAFlag())
				{
					block->SetBlockType(EDiscover);
					if (block->GetBombCount() == 0)
						block->Discover();
				}
			}
		}
	}
}

void Block::Flag()
{
	bIsAFlag = !bIsAFlag;
}

bool Block::IsAFlag()
{
	return bIsAFlag;
}
