#pragma once

class Game;

enum EBlockType
{
	EHidden,
	EDiscover
};

class Block
{
	public:
		Block();

		void SetGameAndIndex(Game* game, int x, int y);

		void SetBlockType(EBlockType blockType);

		EBlockType GetBlockType();

		void SetAsBomb();

		bool IsABomb();

		int	GetBombCount();

		void UpdateBombCount();

		void Discover();

		void Flag();

		bool IsAFlag();

	private:
		Game *game;
		int x;
		int y;

		EBlockType blockType;

		bool bIsABomb;

		int bombCount;

		bool bIsAFlag;
};
