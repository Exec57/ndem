SRCS_DIR = srcs
OBJS_DIR = objs
INCS_DIR = includes

HEADS = Game.h\
		Block.h
SRCS = Main.cpp\
	   Game.cpp\
	   Block.cpp
OBJS = $(addprefix $(OBJS_DIR)/, $(SRCS:.cpp=.o))

NAME = ndem
RM = rm -rf
CC = g++

INC = -I$(INCS_DIR)
LIB = -lncurses -lstdc++

CFLAGS = -Werror -Wall -Wextra -Ofast

.PHONY: clean debug

all: $(OBJS_DIR) $(NAME)

$(NAME): $(OBJS)
	@$(CC) -o $@ $^ $(INC) $(LIB)

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.cpp $(addprefix $(INCS_DIR)/, $(HEADS))
	@printf "CC \033[1m$<\033[0m\n"
	@$(CC) -c $(INC) $(CFLAGS) -o $@ $<

$(OBJS_DIR):
	@printf "=== $(NAME)\n"
	mkdir $(OBJS_DIR)

clean:
	$(RM) $(OBJS_DIR)

fclean: clean
	$(RM) $(NAME)

debug: CFLAGS = -Ofast -DDEBUG
debug: DEBUG = debug
debug: all

re: fclean all
